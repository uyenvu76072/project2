package com.example.demo.subject;

import java.util.List;


import org.springframework.data.repository.CrudRepository;

public interface SubjectRepository extends CrudRepository<Subject, String> {

	List<Subject> findByTen(String tenMonHoc);
	
	List<Subject> findByBoMon(String boMon);
}
