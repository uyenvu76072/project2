package com.example.demo.subject;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SubjectController {

	@Autowired
	SubjectService subjectService;
	
	@RequestMapping(method = RequestMethod.GET, value = "subject/{maMonHoc}")
	public Optional<Subject> getSubjectInfo(@PathVariable String maMonHoc){
		return subjectService.getSubject(maMonHoc);
	}
	
	
	@RequestMapping(value = "/subject/{maMonHoc}",method = RequestMethod.GET)
	public List<Subject> getAllSubject(){
		return subjectService.getAllSubject();
	}
	
	@RequestMapping(value = "/subject/search", method = RequestMethod.GET)
	  public List<Subject> searchByName(@RequestParam String tenMonHoc) {
	    return subjectService.findStudentByName(tenMonHoc);
	  }
	
	@RequestMapping(value = "/subject/{maMonHoc}",method = RequestMethod.PUT)
	public void updateSubject(@PathVariable String maMonHoc, @RequestBody Subject su) {
		subjectService.updateSubject(maMonHoc, su);
	}
	
	@RequestMapping(value = "/subject/{maMonHoc}",method = RequestMethod.PUT)
	public void createSubject(@PathVariable String maMonHoc, @RequestBody Subject su) {
		subjectService.createSubject(maMonHoc, su);
	}
	
	@RequestMapping(value = "/subject/search_advance", method = RequestMethod.GET)
	  public List<Subject> searchByBoMon(@RequestParam String boMon) {
	    return subjectService.findStudentByName(boMon);
	  }
	
	
	
}

