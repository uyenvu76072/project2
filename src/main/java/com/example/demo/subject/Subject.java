package com.example.demo.subject;


import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@Entity
@EnableAutoConfiguration
@Table(name = "subject1")
public class Subject {

	@Id
	private String maMonHoc;
	private String tenMonHoc;
	private String boMon;
	private String gioiThieu;
	
	
	public String getMaMonHoc() {
		return maMonHoc;
	}
	public void setMaMonHoc(String maMonHoc) {
		this.maMonHoc = maMonHoc;
	}
	public String getTenMonHoc() {
		return tenMonHoc;
	}
	public void setTenMonHoc(String tenMonHoc) {
		this.tenMonHoc = tenMonHoc;
	}
	public String getBoMon() {
		return boMon;
	}
	public void setBoMon(String boMon) {
		this.boMon = boMon;
	}
	public String getGioiThieu() {
		return gioiThieu;
	}
	public void setGioiThieu(String gioiThieu) {
		this.gioiThieu = gioiThieu;
	}
	
	public Subject(String maMonHoc, String tenMonHoc, String boMon, String gioiThieu) {
		this.maMonHoc = maMonHoc;
		this.tenMonHoc = tenMonHoc;
		this.boMon = boMon;
		this.gioiThieu = gioiThieu;
	}
	
	public Subject() {
		maMonHoc = "01";
		tenMonHoc = "Java";
		boMon = "TT";
		gioiThieu = "MonTT";
	}
}
