package com.example.demo.subject;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubjectService {

	@Autowired
	SubjectRepository subjectRepository;
	
	public Optional<Subject> getSubject(String maMonHoc){
		return subjectRepository.findById(maMonHoc);
	}
	
	public List<Subject> getAllSubject() {
		// TODO Auto-generated method stub
		List<Subject> subject = new ArrayList<Subject>();
		subjectRepository.findAll().forEach(subject::add);
		return subject;
	}

	public void updateSubject(String maMonHoc, Subject su) {
		// TODO Auto-generated method stub
		su.setMaMonHoc(maMonHoc);
		subjectRepository.save(su);
		
	}

	public void createSubject(String maMonHoc, Subject su) {
		// TODO Auto-generated method stub
		su.setMaMonHoc(maMonHoc);
		subjectRepository.save(su);
	}

	public List<Subject> findStudentByName(String tenMonHoc) {
		// TODO Auto-generated method stub
		List<Subject> subject = new ArrayList<>();
		subjectRepository.findByTen(tenMonHoc).forEach(subject::add);
		return subject;
	}
	
	public List<Subject> findStudentByBoMon(String boMon) {
		// TODO Auto-generated method stub
		List<Subject> subject = new ArrayList<>();
		subjectRepository.findByTen(boMon).forEach(subject::add);
		return subject;
	}
	 
}
